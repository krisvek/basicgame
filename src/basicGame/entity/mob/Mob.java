package basicGame.entity.mob;

import basicGame.Game;
import basicGame.entity.Entity;
import basicGame.entity.projectile.PewProjectile;
import basicGame.entity.projectile.Projectile;
import basicGame.graphics.Sprite;

public abstract class Mob extends Entity { // mob ~= mobile

	protected Sprite sprite;
	protected int dir = -1;
	protected double speed;
	protected double oldX, oldY;
	protected double newX, newY;
	protected boolean moving = false;
	protected double moveDist;
	protected double moveDirDegrees;
	protected Projectile currentPew = new PewProjectile(x, y);

//	protected List<Projectile> projectiles = new ArrayList<Projectile>();

//	public void move() { // xa ~= x-delta?
//
//	}
	
	public void move(int xa, int ya) { // xa ~= x-delta?
		if (ya < 0 && xa == 0)
			dir = 0;// N
		if (xa > 0 && ya < 0)
			dir = 1;// NE
		if (xa > 0 && ya == 0)
			dir = 2;// E
		if (xa > 0 && ya > 0)
			dir = 3;// SE
		if (ya > 0 && xa == 0)
			dir = 4;// S
		if (xa < 0 && ya > 0)
			dir = 5;// SW
		if (xa < 0 && ya == 0)
			dir = 6;// W
		if (xa < 0 && ya < 0)
			dir = 7;// NW

		// if (xa != 0 && ya != 0) {
		// move(xa, 0);
		// move(0, ya);
		// return;
		// }

		if (!collision(xa, 0)) {
			// -1, 0, 1
			x += xa * speed;
			// y += ya*8;
			if (!collision(0, ya)) {
				// -1, 0, 1
				// x += xa*8;
				y += ya * speed;
			}
		}

	}

	public void mouseMove(int mouseX, int mouseY) { 
		oldX = mouseX;
		oldY = mouseY;
		double dx = mouseX - Game.getScaledWidth() / 2;
		double dy = mouseY - Game.getScaledHeight() / 2;
		double moveDir = Math.atan2(dy, dx);
		moveDirDegrees = Math.toDegrees(moveDir) + 180;
		moveDist = Math.sqrt(Math.abs(Math.pow(dy, 2) + Math.pow(dx, 2))); 

		System.out.println("Angle: " + moveDirDegrees);
//		System.out.println("Radian: " + moveDir);
		System.out.println("Distance: " + moveDist);

		newX = speed * Math.cos(moveDir);
		newY = speed * Math.sin(moveDir);

		System.out.println("newX: " + newX + " newY: " + newY);

	}

	public void update() {
		
	}

	protected void shoot(int x, int y, double shootDir) {
		// shootDir = Math.toDegrees(shootDir);
//		 System.out.println("Angle: " + shootDir);

		currentPew = new PewProjectile(x, y, shootDir);
		level.entities.add(currentPew);
		currentPew.init(level);

	}

	public boolean collision(double newX, double newY) {
		boolean solid = false;
		for (int c = 0; c < 4; c++) {
			double xt = ((x + newX) + c % 2 * 32 - 32) / 64;
			double yt = ((y + newY) + c / 2 * 32 - 32) / 64;
			if (level.getTile((int)xt, (int)yt).solid())
				solid = true;
		}
		return solid;
	}

	public void render() {

	}

}
