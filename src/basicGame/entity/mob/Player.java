package basicGame.entity.mob;

import basicGame.Game;
import basicGame.entity.projectile.Projectile;
import basicGame.graphics.Screen;
import basicGame.graphics.Sprite;
import basicGame.graphics.SpriteSheet;
import basicGame.input.Keyboard;
import basicGame.input.Mouse;

public class Player extends Mob {

	private Keyboard input;
	public Sprite sprite;
	public Sprite elevatedSprite;
	private int anim = 0;
	private boolean moving = false;
	private double fireRate;
//	private Projectile currentPew = new PewProjectile(anim, anim, fireRate);;

	// public Player(Keyboard input) {
	// this.input = input;
	// sprite = Sprite.comcenE1;
	// // sprite = Sprite.comcen[40];
	//
	// }

	public Player(int x, int y, Keyboard input) {
		this.x = x;
		this.y = y;
		this.input = input;
		buildAnims();
		// sprite = Sprite.comcenE1;
		sprite = Sprite.comcen[40];
		speed = 2;
		fireRate = currentPew.firePeriod;
	}

	protected void move() {
		double dx = oldX - this.x;
		double dy = oldY - this.y; // oldY - Game.getScaledHeight() / 2
		double newDist = Math.sqrt(Math.pow(dy, 2) + Math.pow(dx, 2));

//		System.out.println("newDist: " + newDist);
//		System.out.println("oldX: " + oldX + " oldY: " + oldY);
//		System.out.println("this.x: " + this.x + " this.y: " + this.y);
		if (newDist > 5) {
			if (!collision(newX, 0)) {
				x += newX;
				moving = true;
				if (!collision(0, newY)) {
					y += newY;
					moving = true;
				}
			}
		}
		else
			moving = false;

		if (337.5 <= moveDirDegrees || moveDirDegrees < 22.5)
			dir = 6;// W
		if (22.5 <= moveDirDegrees && moveDirDegrees < 67.5)
			dir = 7;// NW
		if (67.5 <= moveDirDegrees && moveDirDegrees < 112.5)
			dir = 0;// N
		if (112.5 <= moveDirDegrees && moveDirDegrees < 157.5)
			dir = 1;// NE
		if (157.5 <= moveDirDegrees && moveDirDegrees < 202.5)
			dir = 2;// E
		if (202.5 <= moveDirDegrees && moveDirDegrees < 247.5)
			dir = 3;// SE
		if (247.5 <= moveDirDegrees && moveDirDegrees < 292.5)
			dir = 4;// S
		if (292.5 <= moveDirDegrees && moveDirDegrees < 337.5)
			dir = 5;// SW

	}

	public void update() {
		clear();
		
		if (fireRate > 0) fireRate--;
		else fireRate = currentPew.firePeriod;
//		System.out.println("fireRate: " + fireRate);

		if (Mouse.getButton() == 3) {
			updateMouseMoving();
			// moving = true;
		} else if (input.up || input.down || input.left || input.right) {
			updateKeyMoving();
			// moving = true;
		}
		// else moving = false;

		move();

		updateShooting();

		if (anim < 1000 && moving)
			anim++;
		else if (anim >= 1000)
			anim = 0;

	}

	private void clear() {
		for (int i = 0; i < level.entities.size(); i++) {
			Projectile p = (Projectile) level.entities.get(i);
			if (p.isRemoved())
				level.entities.remove(i);
		}

	}

	private void updateMouseMoving() {
		mouseMove(Mouse.getX(), Mouse.getY());

	}

	private void updateKeyMoving() {
		int xa = 0, ya = 0;
		//
		if (input.up)
			ya--;
		if (input.down)
			ya++;
		if (input.left)
			xa--;
		if (input.right)
			xa++;

		if (xa != 0 || ya != 0) {
			move(xa, ya);
			moving = true;
		} else {
			moving = false;
		}

	}

	private void updateShooting() {
//		System.out.println("fireRate: " + fireRate);
		if (Mouse.getButton() == 1 &&  fireRate == 0) {
			double dx = Mouse.getX() - Game.getScaledWidth() / 2; // x
																	// //Screen.width/2
			double dy = Mouse.getY() - Game.getScaledHeight() / 2; // y
																	// //Screen.height/2
			double shootDir = Math.atan2(dy, dx);
			double shootDist = Math.sqrt(Math.pow(dy, 2) + Math.pow(dx, 2)); // dy
																				// /
																				// Math.sin(shootDir);

			double shootDirDegrees = Math.toDegrees(shootDir);
//			System.out.println("Angle: " + shootDirDegrees);
//			System.out.println("Distance: " + shootDist);

			shoot(x, y, shootDir);
		}
	}

	public void render(Screen screen) {
		if (dir == 0 && moving)
			sprite = Sprite.comcen[(anim % 50) / 5 + 20]; // N
		if (dir == 1 && moving)
			sprite = Sprite.comcen[(anim % 50) / 5 + 30]; // NE
		if (dir == 2 && moving)
			sprite = Sprite.comcen[(anim % 50) / 5 + 40]; // E
		if (dir == 3 && moving)
			sprite = Sprite.comcen[(anim % 50) / 5 + 50]; // SE
		if (dir == 4 && moving)
			sprite = Sprite.comcen[(anim % 50) / 5 + 60]; // S
		if (dir == 5 && moving)
			sprite = Sprite.comcen[(anim % 50) / 5 + 70]; // SW
		if (dir == 6 && moving)
			sprite = Sprite.comcen[(anim % 50) / 5 + 0]; // W
		if (dir == 7 && moving)
			sprite = Sprite.comcen[(anim % 50) / 5 + 10]; // NW

		if (level.getTile((x) / 64, (y) / 64).elevated()) { // /64
			// TODO: make unit appear larger on elevation

			// System.out.println("ELEVATED!");
			screen.renderPlayer(x - sprite.X_SIZE / 2, y - sprite.Y_SIZE / 2,
					sprite);
		} else {
			// System.out.println("not elevated");
			screen.renderPlayer(x - sprite.X_SIZE / 2, y - sprite.Y_SIZE / 2,
					sprite);
		}

	}

	private void buildAnims() {
		int i = 0;

		for (int sheety = 0; sheety < 8; sheety++) {
			for (int sheetx = 0; sheetx < 11; sheetx++) {
				Sprite.comcen[i] = new Sprite(56, 56, sheetx, sheety,
						SpriteSheet.comcenAnim); // 56
				i++;
				if (i == 80)
					break;
			}
			if (i == 80)
				break;
		}
	}

}