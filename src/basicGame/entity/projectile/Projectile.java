package basicGame.entity.projectile;

import basicGame.entity.Entity;
import basicGame.graphics.Sprite;

public class Projectile extends Entity {
	protected int xOrigin, yOrigin; 
	protected double angle;
	protected Sprite sprite;
	protected double x, y;
	protected double newX, newY;
	protected double distance;
	public double speed, firePeriod, range, damage;
	
	public Projectile(int x, int y) {
		xOrigin = x;
		yOrigin = y;
		this.x = x;
		this.y = y;
	}
	
	public Projectile(int x, int y, double shootDir) {
		xOrigin = x;
		yOrigin = y;
		angle = shootDir;
		this.x = x;
		this.y = y;
	}
	
	public Sprite getSprite() {
		return sprite;
	}
	
	protected void move() {
		
	}
}
