package basicGame.entity.projectile;

import basicGame.graphics.Screen;
import basicGame.graphics.Sprite;

public class PewProjectile extends Projectile {
	// private double fireRate;

	public PewProjectile(int x, int y) {
		super(x, y);
		this.x = x;
		this.y = y;
		// range = 5;
		// speed = 5;
		// damage = 5;
		// firePeriod = 5; // controls rate of fire
		// sprite = Sprite.pewProjectile;
	}

	public PewProjectile(int x, int y, double dir) {
		super(x, y, dir);
		// this.x = x;
		// this.y = y;
		range = 200;
		speed = 2;
		damage = 20;
		firePeriod = 5; // controls rate of fire, higher number is slower rate
		sprite = Sprite.pewProjectile;

		newX = speed * Math.cos(angle);
		newY = speed * Math.sin(angle);
	}

	public void update() {
		move();
	}

	protected void move() {
		if (!level.tileCollision(x, y, newX, newY, 32)) {
			x += newX;
			y += newY;
		}
		else
			remove();
		
		if (distance() > range)
			remove();
		
		System.out.println("entities: " + level.entities.size());
	}

	private double distance() {
		double dist = 0;
		dist = Math.sqrt(Math.abs(Math.pow(xOrigin - x, 2) + Math.pow(yOrigin - y, 2)));

		return dist;
	}

	public void render(Screen screen) {
		screen.renderProjectile((int) x - 28, (int) y - 28, this);
	}

}
