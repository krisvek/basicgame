package basicGame.level;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

import basicGame.graphics.Sprite;
import basicGame.graphics.SpriteSheet;
import basicGame.level.tile.GrassTile;
import basicGame.level.tile.HillTile;
import basicGame.level.tile.Tile;
import basicGame.level.tile.WallTile;

public class TextLevel extends Level {

	public TextLevel(String path) {
		super(path);
	}

	protected void loadLevel(String path) {
		final URL url = getClass().getResource(path);

		if (url == null) // Resource does not exist
			System.out.println("Exception! Could not load level file!");

		final InputStream in;
		final Scanner scan;

		try {
			in = url.openStream();
			scan = new Scanner(in);

			width = 55;
			height = 55;
			tiles = new int[width * height];

			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					tiles[x + y * width] = scan.nextInt();
				}
			}
			scan.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Exception! Could not load level file!");
		}
	}

	protected void generateLevel() {
		int i = 0;

		for (int sheety = 0; sheety < 7; sheety++) { // patches 0 through 69
			for (int sheetx = 0; sheetx < 10; sheetx++) {
				if (i == 0) {
					Tile.patches[i] = new GrassTile(new Sprite(64, 64, sheetx, sheety,
							SpriteSheet.sheet1));
				}
				else if (i == 1){
					Tile.patches[i] = new WallTile(new Sprite(64, 64, sheetx, sheety,
							SpriteSheet.sheet1));
				}
				else
					Tile.patches[i] = new Tile(new Sprite(64, 64, sheetx, sheety,
							SpriteSheet.sheet1));
					
				i++;
			}
		}
		for (int sheety = 0; sheety < 7; sheety++) { // patches 70 through 139
			for (int sheetx = 0; sheetx < 10; sheetx++) {
				if ((i >= 104 && i <= 109) || (i >= 114 && i <= 119) ||
						(i >= 123 && i <= 129) || (i >= 132 && i <= 139)) {
					Tile.patches[i] = new WallTile(new Sprite(64, 64, sheetx, sheety,
							SpriteSheet.sheet2));
				} 
				else
					Tile.patches[i] = new Tile(new Sprite(64, 64, sheetx, sheety,
							SpriteSheet.sheet2));

				i++;
			}
		}
		for (int sheety = 0; sheety < 7; sheety++) { // patches 140 through 209
			for (int sheetx = 0; sheetx < 10; sheetx++) {
				Tile.patches[i] = new WallTile(new Sprite(64, 64, sheetx, sheety,
						SpriteSheet.sheet3));
				i++;
			}
		}
		for (int sheety = 0; sheety < 7; sheety++) { // patches 210 through 279
			for (int sheetx = 0; sheetx < 10; sheetx++) {
				Tile.patches[i] = new WallTile(new Sprite(64, 64, sheetx, sheety,
						SpriteSheet.sheet4));
				i++;
			}
		}
		for (int sheety = 0; sheety < 7; sheety++) { // patches 280 through 349
			for (int sheetx = 0; sheetx < 10; sheetx++) {
				if (i == 280 || i == 284 || i == 320 || i == 324){
					Tile.patches[i] = new GrassTile(new Sprite(64, 64, sheetx, sheety,
							SpriteSheet.sheet5));
				}				
				else if ((i > 280 && i < 283) || (i >= 285 && i <= 287) || (i >= 290 && i <= 297) ||
						(i >= 300 && i <= 309) || (i >= 310 && i <= 314) || (i >= 318 && i <= 319) ||
						(i >= 321 && i <= 323)) {
					Tile.patches[i] = new HillTile(new Sprite(64, 64, sheetx, sheety,
							SpriteSheet.sheet5));
				}
				else
					Tile.patches[i] = new Tile(new Sprite(64, 64, sheetx, sheety,
							SpriteSheet.sheet5));
				
				i++;
			}
		}
		for (int sheety = 0; sheety < 3; sheety++) { // patches 0 through 69
			for (int sheetx = 0; sheetx < 10; sheetx++) {
				Tile.patches[i] = new Tile(new Sprite(64, 64, sheetx, sheety,
						SpriteSheet.sheet6));
				i++;
				if (i == 363)
					break;
			}
			if (i == 363)
				break;
		}
	}

}
