package basicGame.level;

import java.util.ArrayList;
import java.util.List;

import basicGame.entity.Entity;
import basicGame.graphics.Screen;
import basicGame.level.tile.Tile;

public class Level {

	protected int width, height;
	protected int[] tiles;
	
	public List<Entity> entities = new ArrayList<Entity>();

	public Level(int width, int height) {
		this.width = width;
		this.height = height;
		tiles = new int[width * height];
		generateLevel();
	}

	public Level(String path) {
		loadLevel(path);
		generateLevel();
	}

	protected void loadLevel(String path) {

	}

	protected void generateLevel() {
		
	}

	public void update() {
		for (int i = 0; i < entities.size(); i++) {
			if (entities.get(i).isRemoved())
				entities.remove(i);
			else
				entities.get(i).update();
		}

	}

	private void time() {

	}
	
	public boolean tileCollision(double x, double y, double newX, double newY, int size) {
		boolean solid = false;
		for (int c = 0; c < 4; c++) {
			double xt = ((x + newX) + c % 2 * size) / 64;
			double yt = ((y + newY) + c / 2 * size) / 64;
			if (getTile((int)xt, (int)yt).solid())
				solid = true;
		}
		return solid;
	}

	public void render(int xScroll, int yScroll, Screen screen) {
		screen.setOffset(xScroll, yScroll);
		// four corner-pins
		int x0 = xScroll >> 6; // /16 >>4
		int x1 = (xScroll + screen.width + 64) >> 6;// +16
		;
		int y0 = yScroll >> 6;// >>4 = 2^4
		int y1 = (yScroll + screen.height + 64) >> 6;// +16 >> 4

		// render each tile
		for (int y = y0; y < y1; y++) {
			for (int x = x0; x < x1; x++) {
				getTile(x, y).render(x, y, screen);
			}
		}
		
		for (int i = 0; i < entities.size(); i++) {
			entities.get(i).render(screen);
		}
	}
	
	public void add(Entity e) {
		entities.add(e);
	}

	public Tile getTile(int x, int y) {		
		for (int i = 0; i < 363; i++) {//363
			if (x < 0 || y < 0 || x >= width || y >= height)
				return Tile.patches[0];
			if (tiles[x + y * width] == i)
				return Tile.patches[i];			
		}
		return Tile.patches[0];
	}

}
