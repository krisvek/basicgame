package basicGame.level.tile;

import basicGame.graphics.Screen;
import basicGame.graphics.Sprite;

public class Tile {

	public int x, y;
	public Sprite sprite;
	
	public static Tile[] patches = new Tile[363];
	
	public Tile() {
				
	}
	
	public Tile(Sprite sprite) {
		this.sprite = sprite;
				
	}
		
	public void render(int x, int y, Screen screen) {
		// added for TextMap
		screen.renderTile(x << 6, y << 6, this); //<< 4 << 4
	}
	
	public boolean solid() {
		return false;
	}
	
	public boolean elevated() {
		return false;
	}
	
	public boolean covered() { // or hidden?
		return false;
	}
}
