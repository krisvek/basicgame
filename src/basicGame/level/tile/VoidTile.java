package basicGame.level.tile;

import basicGame.graphics.Screen;
import basicGame.graphics.Sprite;
//import basicGame.graphics.SpriteCherno;

public class VoidTile extends Tile {

	public VoidTile(Sprite sprite) {
		super(sprite);
	}
	
	public void render(int x, int y, Screen screen) {
		screen.renderTile(x << 6, y << 6, this);
	}

}
