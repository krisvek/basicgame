package basicGame;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import javax.swing.JFrame;

import basicGame.entity.mob.Player;
import basicGame.graphics.Screen;
import basicGame.input.Keyboard;
import basicGame.input.Mouse;
import basicGame.level.Level;
import basicGame.level.TextLevel;
import basicGame.level.TileCoordinate;

public class Game extends Canvas implements Runnable{
	
	private static final long serialVersionUID = 1L;
	private static int width = 1500;
	private static int height = width / 16 * 9;
	private static int scale = 1;
	
	private Thread thread;
	private JFrame frame;
	private Keyboard key;
	private Level level;
	private Player player;
	private boolean running = false;
	
	private Screen screen;
	
	// creating an image
	private BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	// accessing an image
	private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
	
	public Game() {
		Dimension size = new Dimension(width * scale, height * scale);
		setPreferredSize(size);
		
		screen = new Screen(width, height);
		frame = new JFrame();
		key = new Keyboard();	
		//level = new RandomLevel(64, 64);
//		level = new SpawnLevel("/maps/map1.png");
		level = new TextLevel("/maps/map.txt");
		
		TileCoordinate playerSpawn = new TileCoordinate(55, 55);
//		player = new Player(55*64/2, 55*64/2, key); // 6 * 16, 4 * 16
		player = new Player(playerSpawn.x()/2, playerSpawn.y()/2, key);
		player.init(level);
		
		addKeyListener(key);		
		Mouse mouse = new Mouse();
		addMouseListener(mouse);
		addMouseMotionListener(mouse);
	}
	
	public static int getScaledWidth() {
		return width * scale;
	}
	
	public static int getScaledHeight() {
		return height * scale;
	}
	
	public synchronized void start() {
		running = true;
		thread = new Thread(this, "Display");
		thread.start();
	}
	
	public synchronized void stop() {
		running = false;
		try {
			thread.join();
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}

	// never called, because implements Runnable
	@Override
	public void run() {
		long lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		final double ns = 1000000000.0 / 60.0; // [1e9nanosec/1sec * 1sec/60updates = 16,666,666nanosec/update]
		double delta = 0;
		int frames = 0;
		int updates = 0;
		
		requestFocus();
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				update();// limited to 60 times a second
				updates++;
				delta--;
			}
			render(); // unlimited
			frames++;
			
			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
//				System.out.println(updates + " ups, " + frames + " fps");
				frame.setTitle("basicGame | " + updates + " ups, " + frames + " fps");
				updates = 0;
				frames = 0;
			}
		}
		stop();
	}
	
//	int x, y = 0;
	public void update() {
		key.update();
		player.update();
		level.update();
	}
	
	public void render() {
		BufferStrategy bs = getBufferStrategy();
		if (bs==null) {
			createBufferStrategy(3);
			return;
		}
		screen.clear();
		//screen.render(x,y);
		int xScroll = player.x - screen.width /2;
		int yScroll = player.y - screen.height /2;
		level.render(xScroll, yScroll, screen);
		player.render(screen);
		
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = screen.pixels[i];
		}
		
		Graphics g = bs.getDrawGraphics();
		//g.setColor(new Color(60,0,60));
		//g.fillRect(0, 0, getWidth(), getHeight());
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		g.setColor(Color.BLACK);
		g.setFont(new Font("Verdana", 0, 20));
//		g.fillRect(Mouse.getX() - 32, Mouse.getY() - 32, 64, 64);
		g.drawString("Button: " + Mouse.getButton(), Mouse.getX(), Mouse.getY());
		
		g.drawString("X: " + player.x + ", Y: " + player.y, 0, 20);
		g.dispose();		
		bs.show();
	}
	
	public static void main(String[] args) {
		Game game = new Game();
		game.frame.setResizable(false);
		game.frame.setTitle("basicGame");
		game.frame.add(game);
		game.frame.pack();
		game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		game.frame.setLocationRelativeTo(null);
		game.frame.setVisible(true);
		
		game.start();
	}

}
