package basicGame.graphics;

public class Sprite {
	
	public int X_SIZE; //final
	public int Y_SIZE; //final
	private int x, y;
	public int[] pixels;
	private SpriteSheet sheet;
	
	public static Sprite[] comcen = new Sprite[80];
	public static Sprite pewProjectile = new Sprite(48, 48, 0, 0, SpriteSheet.projectiles);
	
	public Sprite(int xsize, int ysize, int x, int y, SpriteSheet sheet) {
		X_SIZE = xsize;
		Y_SIZE = ysize;
		pixels = new int[X_SIZE * Y_SIZE];
		this.x = x * X_SIZE;
		this.y = y * Y_SIZE;
		this.sheet = sheet;				
		load();
	}
	
	public Sprite(int size, int color) {
		X_SIZE = size;
		Y_SIZE = size;
		pixels = new int[X_SIZE * Y_SIZE];
		setColor(color);
	}
	
	private void setColor(int color) {
		for (int i = 0; i < X_SIZE * Y_SIZE; i++) {
			pixels[i] = color;
		}		
	}
	
	private void load() {
		for (int y = 0; y < Y_SIZE; y++) {
			for (int x = 0; x < X_SIZE; x++) {
				pixels[x + y * X_SIZE] = sheet.pixels[(x + this.x) + (y + this.y) * sheet.X_SIZE];
				
			}
		}
	}

}
