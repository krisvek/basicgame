package basicGame.graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class SpriteSheet {

	private static String path;
	// public final int SIZE;
	public int X_SIZE;
	public int Y_SIZE;
	public int[] pixels;

	// public static SpriteSheet tiles = new SpriteSheet(
	// "/textures/spritesheet1.png", 256, 256);
	public static SpriteSheet sheet1 = new SpriteSheet("/textures/sheet1.png", 640, 448);
	public static SpriteSheet sheet2 = new SpriteSheet("/textures/sheet2.png", 640, 448);
	public static SpriteSheet sheet3 = new SpriteSheet("/textures/sheet3.png", 640, 448);
	public static SpriteSheet sheet4 = new SpriteSheet("/textures/sheet4.png", 640, 448);
	public static SpriteSheet sheet5 = new SpriteSheet("/textures/sheet5.png", 640, 448);
	public static SpriteSheet sheet6 = new SpriteSheet("/textures/sheet6.png", 640, 448);

	public static SpriteSheet comcenAnim = new SpriteSheet("/units/comcen-anim.png", 616, 448);
	public static SpriteSheet comcenRot = new SpriteSheet("/units/comcen-rot.png", 616, 168);
	
	public static SpriteSheet projectiles = new SpriteSheet("/effects/projectiles.png", 624, 480);

	public SpriteSheet(String path, int x, int y) {
		this.path = path;
		X_SIZE = x;
		Y_SIZE = y;
		pixels = new int[X_SIZE * Y_SIZE];
		load();
	}

	private void load() {
		try {
			BufferedImage image = ImageIO.read(SpriteSheet.class.getResource(path));
			int w = image.getWidth();
//			System.out.print("image width: " + w);
			int h = image.getHeight();
//			System.out.println(", image height: " + h);
			image.getRGB(0, 0, w, h, pixels, 0, w);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
