package basicGame.graphics;

import basicGame.entity.projectile.Projectile;
import basicGame.level.tile.Tile;

//import java.util.Random;

public class Screen {

	public static int width;
	public static int height;
	public int[] pixels;
//	public final int MAP_SIZE = 8;//8
//	public final int MAP_SIZE_MASK = MAP_SIZE - 1;
	//public int[] tiles = new int[MAP_SIZE * MAP_SIZE];	
	//private Random random = new Random();
	public int xOffset, yOffset;
	
	public Screen(int width, int height) {
		this.width = width;
		this.height = height;
		// create an int for each pixel in the screen
		pixels = new int[width * height];
		
//		for (int i = 0; i < MAP_SIZE * MAP_SIZE; i++) {
//			tiles[i] = random.nextInt(0xffffff);
//		}
	}
	
	public void clear() {
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = 0;
		}
	}
	
	public void renderTile(int xp, int yp, Tile tile) {
		xp -= xOffset;
		yp -= yOffset;
		for (int y = 0; y < tile.sprite.Y_SIZE; y++) {
			int ya = y + yp; // y-absolute (offset) = ya, y-position = yp 
			for (int x = 0; x < tile.sprite.X_SIZE; x++) {
				int xa = x + xp;
				if (xa < -tile.sprite.X_SIZE || xa >= width || ya < 0 || ya >= height) break; //break
				if (xa < 0) xa = 0;
				
				int col = tile.sprite.pixels[x + y * tile.sprite.X_SIZE];//Y
				int altCol = tile.patches[0].sprite.pixels[x + y * tile.sprite.X_SIZE];// grass tile
				if (col != 0xffffffff) {
					pixels[xa + ya * width] = col; // if the color isn't white, draw it
				}
				else {
					pixels[xa + ya * width] = altCol; // if it is white, draw the pixels from the grass tile instead
				}				
//				pixels[xa + ya * width] = tile.sprite.pixels[x + y * tile.sprite.X_SIZE];//Y
			}
		}
	}
	
	public void renderProjectile(int xp, int yp, Projectile p) {
		xp -= xOffset;
		yp -= yOffset;
		for (int y = 0; y < p.getSprite().Y_SIZE; y++) { //16
			int ya = y + yp; // y-absolute (offset) = ya, y-position = yp 
			for (int x = 0; x < p.getSprite().X_SIZE; x++) {
				int xa = x + xp;
				if (xa < -p.getSprite().X_SIZE || xa >= width || ya < 0 || ya >= height) break; //break
				if (xa < 0) xa = 0;
				
				int col = p.getSprite().pixels[x + y * p.getSprite().X_SIZE];//Y
				if (col != 0xffffffff) pixels[xa + ya * width] = col;
			}
		}
	}
	
	public void renderPlayer(int xp, int yp, Sprite sprite) {
		xp -= xOffset;
		yp -= yOffset;
		for (int y = 0; y < sprite.Y_SIZE; y++) { //16
			int ya = y + yp; // y-absolute (offset) = ya, y-position = yp 
			for (int x = 0; x < sprite.X_SIZE; x++) {
				int xa = x + xp;
				if (xa < -sprite.X_SIZE || xa >= width || ya < 0 || ya >= height) break; //break
				if (xa < 0) xa = 0;
				
				int col = sprite.pixels[x + y * sprite.X_SIZE];//Y
				if (col != 0xffffffff) pixels[xa + ya * width] = col;
				//pixels[xa + ya * width] = col;
			}
		}
	}
	
	public void setOffset(int xOffset, int yOffset) {
		this.xOffset = xOffset;
		this.yOffset = yOffset;
		
	}
}
